import { Injectable, Component, Directive, OnInit, ElementRef, Input, NgZone } from '@angular/core';

import * as $ from 'jquery'
import * as _ from 'underscore'
import { IDndAdapter } from '../services/Interfaces'

@Injectable()
export class DndManager {

    public draggedData : any;

    public fromList : any[];

    public toList : any[];

    public fromIndex : number;

    public sortableTypeName : string;

    public asSortableTypeName : string;

    public lastMovedId : string;

    constructor(){
        this["_id_"] = _.uniqueId("dndMgr_");
        console.log(this["_id_"]);
    }

    public cloneDraggedData(converterFunc: (data: any) => any = null){
        if (this.draggedData == null){
            return null;
        }

        let data = this.clone(this.draggedData);

        if (converterFunc){
            data = converterFunc(data);
        }

        return data;
    }

    public onDragstart(event: DragEvent, dataItem: any, sortableTypeName: string){
        event.stopPropagation();
        event.dataTransfer.setData("text", JSON.stringify(dataItem));
        this.draggedData = dataItem;
        dataItem.isBeingDragged = true;
        dataItem.isPlaceholder = true;
        dataItem.uniqueId = dataItem.uniqueId || _.uniqueId("dndItem_");
        this.sortableTypeName = sortableTypeName;
    }

    public onDragend(){
        if (this.draggedData){
            this.draggedData.isBeingDragged = false;
        }
        this.lastMovedId = null;
        this.fromList = null;
        this.toList = null;
        this.sortableTypeName = null;
        this.fromIndex = -1;
    }

    public onDrop(event: DragEvent, dndAdapter: IDndAdapter, acceptTypeNames: string[], dataList : any[]){
        const currentTypeName = this.sortableTypeName;
        if (!this.isAcceptableBy(acceptTypeNames, currentTypeName)) {
            return;
        }
        
        event.stopPropagation();

        const placeholderIndex = this.findPlaceholderIndex(dataList);

        if (placeholderIndex > -1) {
            this.updateListItemUsingDraggedData(dataList, placeholderIndex, event, dndAdapter ? dndAdapter.convertDataForDrop : null);
        }

        this.draggedData.isBeingDragged = false;
        this.draggedData = null;
    }

    public allowDropToList(event: DragEvent, dataList: any[]){
        event.stopPropagation();
        event.preventDefault();
    }

    public checkAllowDrop(event: DragEvent, dndAdapter: IDndAdapter, acceptTypeNames: string[], dataList: any[], dataItem: any): boolean{
        const currentTypeName = this.sortableTypeName;
        if (!this.isAcceptableBy(acceptTypeNames, currentTypeName) || (dndAdapter && !dndAdapter.isDropAllowed(event, dataList, dataItem))) {
            return false;
        }

        this.allowDropToList(event, dataList);

        this.switchTargetList(dataList);

        return true;
    }
    
    public switchTargetList(dataList: any[]) {
        
        if (this.toList && this.toList !== dataList) {         
            if(this.removePlaceholder(this.toList)){
                console.log("causing flashing issue ..");  
            }
        }

        this.toList = dataList;
    }

    public isAcceptableBy(acceptTypes: string[], typeName: string) : boolean {        
        return acceptTypes.indexOf(typeName) > -1;
    }

    public findPlaceholderIndex(list: any[]){
        return _.findIndex(list, (x: any) => x.isPlaceholder);
    }

    public insertToList(list: any[], index: number, newItem: any){
        list.splice(index, 0, newItem);
    }

    public removePlaceholder(list: any[], predicate: (x: any) => boolean = null) : any {
        let placeholderIndex = this.findPlaceholderIndex(list);

        if (placeholderIndex > -1){
            let placeholder = list[placeholderIndex];
             
            list.splice(placeholderIndex, 1);
            // this.lastMovedId = placeholder.uniqueId;

            return placeholder;
        }

        return null;
    }

    public clone(obj: any) : any {
        if (obj == null){
            return null;
        }

        return JSON.parse(JSON.stringify(obj));
    }

    public move(arr: any[], fromIndex: number, toIndex: number) {
        if (fromIndex >= arr.length) {
            let k = toIndex - arr.length;
            while ((k--) + 1) {
                arr.push(undefined);
            }
        }
        arr.splice(toIndex, 0, arr.splice(fromIndex, 1)[0]);
    }

    public moveToAnotherList(fromList: any[], fromIndex: number, toList: any[], toIndex: number){
        const item = fromList[fromIndex];
        fromList.splice(fromIndex, 1);
        this.insertToList(toList, toIndex, item);
    }

    public updateListItemUsingDraggedData(list: any[], index: number, event: DragEvent, converter: (x:any) => any, resetPlaceholder: boolean = true, dataType: string = "text"){
        
        let jsonData = this.clone(this.draggedData);
        if (resetPlaceholder){
            jsonData.isPlaceholder = false;
            jsonData.isBeingDragged = false;
        }
        
        if (converter){
            jsonData = converter(jsonData);
        }
        
        list[index] = jsonData;
    }
}