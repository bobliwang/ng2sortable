import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import * as _ from "underscore";


@Component({
  selector: 'image-resizer',
  template: `


      <div>
        <input type="file" id="fileUpload" (change)="load()"/>
      </div>

      <h3>Original Image</h3>
      <div style="clear:both"></div>
      <div>
        
        <div style="float:left; min-width:300px;">
          <img id="image1" [src]="dataUrl" />
        </div>
        <div style="float:left; width: 100px;">
          <input type="text" [(ngModel)]="percent"/>
          <input type="button" value="Resize" (click)="resize()"/>
        </div>
        <div style="float:left; min-width:300px;">          
          <canvas id="canvas1" width="300" height="200"></canvas>
        </div>
      </div>
      <div style="clear:both"></div>
  `,
})
export class ImageResizerComponent extends OnInit {
   
  dataUrl : string;

  percent : number = 50;

  public ngOnInit() : void {
    var canvas = document.getElementById("canvas1") as HTMLCanvasElement;
    var ctx = canvas.getContext("2d");
    ctx.fillText("waiting for resize", 100, 100, 200);
  }

  load() : void {
    const fileUpload : any = document.getElementById('fileUpload');
    const file = fileUpload.files.item(0);
    const fileReader = new FileReader();
    fileReader.onload = (evt) => {
      this.dataUrl = fileReader.result;
    };

    fileReader.readAsDataURL(file);
  }

  resize() : void {
    var canvas = document.getElementById("canvas1") as HTMLCanvasElement;
    var ctx = canvas.getContext("2d");
    var img = document.getElementById("image1") as HTMLImageElement;


    var rate = 1.0 * this.percent / 100;

    canvas.width = img.width * rate;
    canvas.height = canvas.width * (img.height / img.width);

    /// step 1
    var tempCanvas = document.createElement('canvas');
    var tempCanvasCtx = tempCanvas.getContext('2d');

    tempCanvas.width = img.width * rate;
    tempCanvas.height = img.height * rate;
    tempCanvasCtx.drawImage(img, 0, 0, tempCanvas.width, tempCanvas.height);

    // /// step 2
    // // tempCanvasCtx.drawImage(tempCanvas, 0, 0, tempCanvas.width * 0.5, tempCanvas.height * 0.5);

    ctx.drawImage(tempCanvas, 0, 0, tempCanvas.width, tempCanvas.height,
    0, 0, canvas.width, canvas.height);
  }

}
