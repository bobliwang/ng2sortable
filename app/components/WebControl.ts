import { Component, OnInit, Input } from '@angular/core';
import { NgModel } from '@angular/forms';
import * as _ from "underscore";


@Component({
  selector: 'web-control',
  template: `
    <div [ngSwitch]="controlData.Type">
      <div *ngSwitchCase="'Control'">
        {{controlData.Name}}
      </div>
      
      <div *ngSwitchCase="'Repeater'">
        
        <div>{{controlData.Name}}</div>

        <grid-control [grid]="controlData"></grid-control>

      </div>
    </div>
  `,
})
export class WebControl extends OnInit {
   
  @Input("control-data")
  controlData: any;


  public ngOnInit() : void {
  }


}



