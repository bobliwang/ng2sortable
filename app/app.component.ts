import { Component, OnInit } from '@angular/core';
import * as _ from "underscore";
import * as models from './models/Domain.Models';


@Component({
  selector: 'my-app',
  template: `
      <forms-designer></forms-designer>
  `,
})
export class AppComponent extends OnInit {
   
  name = 'Angular';

  listData: any[] = [];

  listData2: any[] = [];

  listData3: any[] = [{
    itemId: _.uniqueId("draggable_")
  }];

  dataUrl : string;

  showSortable: boolean = false;

  grid = new models.Grid();

  fakeControl = new models.ControlProperties();
  fakeRow = new models.Row();


  public ngOnInit() : void {
    
    for(var i = 0; i < 3; i ++){
      this.listData.push({
        itemId: _.uniqueId("list1_")
      });

      this.listData2.push({
        itemId: _.uniqueId("list2_")
      });
    }

    let row = new models.Row();
    row.Columns.push(new models.ControlProperties());
    this.grid.Rows.push(row);
  }

  removeFromList(list: any[], item: any){
    var index = list.indexOf(item);
    if (index > -1){
      list.splice(index, 1);
    }
  }
}
