import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import * as _ from "underscore";


@Component({
  selector: 'my-card',
  template: `
    <div>
      <div>Card Header</div>

      <div>
        <ng-content selector="[card-body]"></ng-content>
      </div>
    </div>
  `,
})
export class MyCardComponent extends OnInit {
   

  public ngOnInit() : void {
  }


}



