import { Component, Directive, OnInit, OnChanges, ElementRef, Input, NgZone } from '@angular/core';
import { DndManager } from '../services/DndManager'
import { SortableList } from './SortableList'
import { IDndAdapter } from '../services/Interfaces'
import * as $ from 'jquery'
import * as _ from 'underscore'


@Directive({
    selector: '[sortable-item]'
})
export class SortableItem implements OnInit, OnChanges {

    @Input("sortable-item")
    private dataItem: any;

    @Input("sortable-dragover-delay")
    private dragoverDelay: number;

    private lastEnteredTimeMs : number;

    private get dataList(): any[]{
        return this.sortableList.dataList;
    }

    private get sortableTypeName(): string{
        return this.sortableList.sortableTypeName;
    }

    private get acceptTypeNames(): string[] {
        return this.sortableList.acceptTypeNames;
    }

    private get dndAdapter() : IDndAdapter {
        return this.sortableList.dndAdapter;
    }

    private get sortableDisabled() : boolean {
        return this.sortableList.sortableDisabled;
    }

    public constructor(private elementRef: ElementRef, private dndManager: DndManager, private zone: NgZone, private sortableList: SortableList) {
    }

    ngOnChanges(changes: any) {
    }

    ngOnInit(): void {
        
        let element = this.elementRef.nativeElement;
        this.dataItem.uniqueId = this.dataItem.uniqueId || _.uniqueId("dndItem_");
        if (this.dataItem.isPlaceholder) {
            $(element).addClass("droppable");
        }
        else {
            $(element).removeClass("droppable");
        }

        $(element).addClass("sortable-item");
        

        if (!this.sortableDisabled){
            this.setupEvents(element);
        }
    }

    private currentItemIndex(): number{
        return this.dataList.indexOf(this.dataItem);
    }

    private dragstart(event: DragEvent) {
        this.dndManager.onDragstart(event, this.dataItem, this.sortableTypeName);
        this.dndManager.fromList = this.dataList;
        this.dndManager.fromIndex = this.currentItemIndex();
    }

    private dragenter(event: DragEvent){
        this.lastEnteredTimeMs = new Date().getTime();
    }

    private dragover(event: DragEvent) {

        if (this.dragoverDelay){
            const now = new Date().getTime();
            if (now - this.lastEnteredTimeMs < this.dragoverDelay){
                event.stopPropagation();
                return;
            }
        }

        console.log("lastMovedId", this.dndManager.lastMovedId, this.dataItem.uniqueId);
        if (this.dndManager.lastMovedId === this.dataItem.uniqueId){
            console.log("dragover - no - 2");
            return;
        }

        if (!this.dndManager.checkAllowDrop(event, this.dndAdapter, this.acceptTypeNames, this.dataList, this.dataItem)){
            console.log("dragover - no - 0");
            return;
        }

        if (this.dataItem.isPlaceholder) {
            console.log("dragover - no - 1");
            return;
        }

        const placeholderIndex = this.dndManager.findPlaceholderIndex(this.dataList);
        const toIndex = this.currentItemIndex();

        // the placeholder element exists in current list
        if (placeholderIndex > -1) {

            this.dndManager.lastMovedId = this.dataList[toIndex].uniqueId;
            this.dndManager.move(this.dataList, placeholderIndex, toIndex);
            
            // no need to call converter because it has been converted in last event.
            this.dataList[toIndex] = this.dndManager.cloneDraggedData(this.dndAdapter ? this.dndAdapter.convertDataForDragover : null);
        }
        else {
            let data = this.dndManager.cloneDraggedData(this.dndAdapter ? this.dndAdapter.convertDataForDragover : null);

            // moving from another list
            if (this.dndManager.fromList) {
                this.dndManager.insertToList(this.dataList, toIndex, data);
            }
            else {
                // from a draggable item
                this.dndManager.insertToList(this.dataList, toIndex, data);
            }
        }
    }
    
    private dragleave(event: DragEvent) {
        this.dndManager.lastMovedId = null;
    }

    private drop(event: DragEvent) {       
        this.dndManager.onDrop(event, this.dndAdapter, this.acceptTypeNames, this.dataList);
    }

    private movePlaceholderBack() {
        let indexForPlaceholder = this.dndManager.findPlaceholderIndex(this.dataList);
        if (indexForPlaceholder > -1) {
            this.dndManager.move(this.dataList, indexForPlaceholder, this.dndManager.fromIndex);

            this.dataList[this.dndManager.fromIndex].isPlaceholder = false;
            this.dataList[this.dndManager.fromIndex] = this.dndManager.clone(this.dataList[this.dndManager.fromIndex]);
        }
    }

    private dragend(event: DragEvent) {
        if (this.dndManager.draggedData) {

            const indexForPlaceholder = this.dndManager.findPlaceholderIndex(this.dndManager.toList)

            if (this.dataList !== this.dndManager.toList && indexForPlaceholder > -1) {
                this.dndManager.toList[indexForPlaceholder].isPlaceholder = false;
                this.dndManager.moveToAnotherList(this.dndManager.toList, indexForPlaceholder, this.dndManager.fromList, this.dndManager.fromIndex);                
            }
            else {
                this.movePlaceholderBack();
            }
        }

        this.dndManager.onDragend();
    }

    private setupEvents(element: HTMLElement) {
        
        $(element).attr("draggable", "true");

        $(element).on("dragstart", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragstart(event.originalEvent as DragEvent);
            });
        });

        $(element).on("dragenter", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragenter(event.originalEvent as DragEvent);
            });
        });

        $(element).on("dragend", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragend(event.originalEvent as DragEvent);
            });
        });

        $(element).on("dragover", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragover(event.originalEvent as DragEvent);
            });
        });

        $(element).on("dragleave", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragleave(event.originalEvent as DragEvent);
            });
        });

        $(element).on("drop", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.drop(event.originalEvent as DragEvent);
            });
        });
    }
}