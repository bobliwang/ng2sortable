import { Component, OnInit, Input } from '@angular/core';
import * as models from '../models/Domain.Models';
import {IDndAdapter} from '../services/Interfaces'
import {DndManager} from '../services/DndManager'
import { NgModel } from '@angular/forms';
import * as _ from "underscore";


@Component({
  selector: 'grid-control',
  template: `
    <div [id]="grid.Id"
      class="grid"
      [sortable-list]="grid.Rows"
      sortable-type="Row"
      sortable-accept-types="Row,Control"
      [sortable-dnd-adapter]="rowDndAdapter"
      [sortable-disabled]="grid.convertedFromControl || grid.isBeingDragged"
      [sortable-dragover-delay]="0">
      
      <div *ngFor="let row of grid.Rows"
          class="canvs-row"
          [sortable-item]="row" [sortable-dragover-delay]="0">

        <div style="min-width:50px; float:left;">
          {{row.Name}}
        </div>

        <div class="controlRowContainer" style="overflow: hidden; min-height:55px;"
          [sortable-list]="row.Columns"
          sortable-type="Control"
          sortable-accept-types="Control,Repeater"
          [sortable-dnd-adapter]="ctrlDndAdapter"
          [sortable-disabled]="row.convertedFromControl"
          [sortable-dragover-delay]="0">

          <div *ngFor="let ctrl of row.Columns" style="min-width:100px; min-height: 50px; border: 1px dotted #ccc; padding: 5px; float:left;"
            [sortable-item]="ctrl" [sortable-dragover-delay]="ctrl.Type === 'Repeater' ? 400 : 0">
            
            <web-control [control-data]="ctrl"></web-control>

          </div>

        </div>
      </div>

    <div>
  `,
})
export class GridControl extends OnInit {
   
  @Input()
  grid: models.Grid;

  constructor(private dndManager: DndManager){
    super();
  }

  public ngOnInit() : void {
  }

  ctrlDndAdapter : IDndAdapter = {
    isDropAllowed: (event: DragEvent, list:any[], overItem: any) => {
      if (overItem && overItem.isPlaceholder){
        return true;
      }

      return _.filter(list, (x: any) => !x.isPlaceholder).length < 4;
    },
    convertDataForDragover: (data: any) => {
      if (!data.Seq){
        data.Seq = models.Grid.nextControlId(this.grid);
      }
      
      data.Name = data.Type + " - " + data.Seq;
      console.log(data.Name);

      return data;
    },
    convertDataForDrop: (data: any) => {
      if (!data.Seq){
        data.Seq = models.Grid.nextControlId(this.grid);
      }
      
      data.Name = data.Type + " - " + data.Seq;
      console.log("dropped", data);

      return data;
    }
  };

  rowDndAdapter : IDndAdapter = {
    isDropAllowed: (event: DragEvent, list:any[], overItem: any) => {

      if(this.dndManager.sortableTypeName !== "Row"){

        if (overItem == null){

          const htmlElement = event.currentTarget as HTMLElement;
          const rect = htmlElement.getBoundingClientRect();

          const sortableHtmlElements = $(htmlElement).children(".sortable-item:not(.droppable)");
          const firstSortableItem = sortableHtmlElements[0];
          const lastSortableItem = sortableHtmlElements[sortableHtmlElements.length - 1];

          if (firstSortableItem && lastSortableItem){
            let result = event.clientY < firstSortableItem.getBoundingClientRect().top
                  || event.clientY > lastSortableItem.getBoundingClientRect().bottom;
            console.log("??? " + result);
            return result;
          }
          else{
            console.log("yes 0");
            return true;
          }
          
        }
        if (overItem && !overItem.isPlaceholder){
            event.stopPropagation();
        }
        
        return false;
      }

      let val = overItem == null || overItem.Type === "Row";
      console.log("rowDndAdapter", val, overItem);
      return val;
    },
    convertDataForDragover: (data: any) => {

      if (data.Type === "Row"){
        return data;
      }

      let row = new models.Row();
        
      if (!data.Seq){
        data.Seq = models.Grid.nextControlId(this.grid);
      }
      
      data.Name = data.Type + " - " + data.Seq;
      data.convertedFromControl = true;
      data.isPlaceholder = true;
      row.Columns.push(data);
      row["isPlaceholder"] = true;
      row["convertedFromControl"] = true;

      console.log("Converting " + data.Type + " to Row");

      return row;
      
    },
    convertDataForDrop: (data: any) => {

      if (data.Type === "Row"){
        return data;
      }
      
      let row = new models.Row();
      if (!data.Seq){
        data.Seq = models.Grid.nextControlId(this.grid);
      }
      
      data.Name = data.Type + " - " + data.Seq;
      row.Columns.push(data);

      return row;
    }
  };
}



