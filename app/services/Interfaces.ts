import { Injectable, Component, Directive, OnInit, ElementRef, Input, NgZone } from '@angular/core';

import * as $ from 'jquery'
import * as _ from 'underscore'

export interface IDndAdapter{
    
    isDropAllowed(event:DragEvent, list:any[], overItem: any): boolean

    convertDataForDragover(data: any) : any;

    convertDataForDrop(data: any) : any;
}