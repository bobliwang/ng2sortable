import { Component, OnInit, Input } from '@angular/core';
import { NgModel } from '@angular/forms';
import * as _ from "underscore";


@Component({
  selector: 'looper',
  template: `
    <h3>looper start</h3>
    <div *ngFor="let item of list">
        <ng-content selector="[looper-body]"></ng-content>
        <hr/>
    </div>
    <h3>looper end</h3>
  `,
})
export class LooperComponent extends OnInit {
  
  @Input()
  list: any[];

  public ngOnInit() : void {
  }


}



