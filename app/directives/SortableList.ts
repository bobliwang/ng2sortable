import { Component, Directive, OnInit, OnChanges, ElementRef, Input, Output, NgZone } from '@angular/core';
import { DndManager } from '../services/DndManager'
import { IDndAdapter } from '../services/Interfaces'
import * as $ from 'jquery'
import * as _ from 'underscore'


@Directive({
    selector: '[sortable-list]'
})
export class SortableList implements OnInit, OnChanges {

    @Input("sortable-list")
    public dataList: any[];

    @Input("sortable-type")
    public sortableTypeName: string;

    @Input("sortable-accept-types")
    public acceptTypes: any;

    @Input("sortable-dnd-adapter")
    public dndAdapter: IDndAdapter

    @Input("sortable-disabled")
    public sortableDisabled : boolean

    @Input("sortable-direction")
    public sortableDirection : string;

    @Input("sortable-dragover-delay")
    private dragoverDelay: number;

    private lastEnteredTimeMs : number;

    public constructor(private elementRef: ElementRef, private dndManager: DndManager, private zone: NgZone) {
    }

    ngOnChanges(changes: any) {
    }

    get acceptTypeNames(): string[] {
        if (this.acceptTypes instanceof Array) {
            return this.acceptTypes as string[];
        }

        if (this.acceptTypes) {
            return _.map(this.acceptTypes.split(','), (x: string) => x.trim());
        }
    }

    public ngOnInit(): void {
        if (!this.sortableDirection){
            this.sortableDirection = "vertical";
        }

        const element = this.elementRef.nativeElement;

        if (!this.sortableDisabled){
            this.setupEvents(element);
        }
    }

    private dragenterParent(event: DragEvent) {
        this.lastEnteredTimeMs = new Date().getTime();
    }

    private dragoverParent(event: DragEvent) {
        
        console.log("dragoverDelay - " + this.dragoverDelay);
        if (this.dragoverDelay){
            const now = new Date().getTime();
            const val = now - this.lastEnteredTimeMs;
            console.log("waiting in list", val, this.dragoverDelay);

            if (val < this.dragoverDelay){
                event.stopPropagation();

                
                return;
            }
        }
        
        if (!this.dndManager.checkAllowDrop(event, this.dndAdapter, this.acceptTypeNames, this.dataList, null)){
            return;
        }

        const placeholderIndex = this.dndManager.findPlaceholderIndex(this.dataList);
        if (placeholderIndex < 0) {
            const data = this.dndManager.cloneDraggedData(this.dndAdapter ? this.dndAdapter.convertDataForDragover : null);
            data.isPlaceholder = true;

            const rect = (event.currentTarget as HTMLElement).getBoundingClientRect();
            
            if (this.dataList.length === 0){
                this.dataList.push(data);
            }
            else {
                const sortableHtmlElements = $(event.currentTarget as HTMLElement).children(".sortable-item:not(.droppable)");
                const firstSortableItem = sortableHtmlElements[0];
                const lastSortableItem = sortableHtmlElements[sortableHtmlElements.length - 1];

                if (sortableHtmlElements.length > 0){
                    if (this.sortableDirection === "vertical"){
                        if (event.clientY > lastSortableItem.getBoundingClientRect().bottom){
                            console.log("append vertical");
                            this.dataList.push(data);
                        }
                        else if (event.clientY < firstSortableItem.getBoundingClientRect().top) {
                            console.log("insert vertical");
                            this.dataList.splice(0, 0, data);
                        }
                    }
                    else {
                        if (event.clientX > lastSortableItem.getBoundingClientRect().right){
                            console.log("append horizontal");
                            this.dataList.push(data);
                        }
                        else if (event.clientX < firstSortableItem.getBoundingClientRect().left) {
                            console.log("insert horizontal");
                            this.dataList.splice(0, 0, data);
                        }
                    }
                }
                else{
                    console.log("add first");
                    this.dataList.push(data);
                }

            }
        }
    }

    private dropParent(event: DragEvent) {
        this.dndManager.onDrop(event, this.dndAdapter, this.acceptTypeNames, this.dataList);
    }

    private setupEvents(element: HTMLElement) {

        $(element).on("dragenter", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragenterParent(event.originalEvent as DragEvent);
            });
        });

        $(element).on("dragover", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragoverParent(event.originalEvent as DragEvent);
            });
        });

        $(element).on("drop", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dropParent(event.originalEvent as DragEvent);
            });
        });

    }
}