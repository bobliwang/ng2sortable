import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.component';

import { SortableItem } from './directives/SortableItem'
import { SortableList } from './directives/SortableList'
import { DraggableItem } from './directives/DraggableItem'
import { FormsDesignerComponent } from './formsDesigner.component'
import { DndManager } from './services/DndManager'
import { ImageResizerComponent } from './components/ImageResizer'
import { MyCardComponent } from './components/MyCard'
import { LooperComponent } from './components/Looper'
import { WebControl } from './components/WebControl'
import { GridControl } from './components/GridControl'

@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [
        AppComponent
      , SortableItem
      , SortableList
      , DraggableItem
      , FormsDesignerComponent
      , ImageResizerComponent
      , MyCardComponent
      , LooperComponent
      , WebControl
      , GridControl
  ],

  providers: [
    DndManager
  ],
  
  bootstrap:    [ AppComponent ]
})
export class AppModule {}
