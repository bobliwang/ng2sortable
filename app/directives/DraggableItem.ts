import { Component, Directive, OnInit, OnChanges, ElementRef, Input, NgZone } from '@angular/core';
import { DndManager } from '../services/DndManager'
import * as $ from 'jquery'
import * as _ from 'underscore'


@Directive({
    selector: '[draggable-item]'
})
export class DraggableItem implements OnInit, OnChanges {

    @Input("draggable-item")
    private dataItem: any;

    @Input("sortable-type")
    private sortableTypeName: string;

    public constructor(private elementRef: ElementRef, private dndManager: DndManager, private zone: NgZone) {
    }

    ngOnChanges(changes: any) {
    }

    ngOnInit(): void {
        this.setupEvents();
    }

    private dragstart(event: DragEvent) {
        this.dndManager.onDragstart(event, this.dataItem, this.sortableTypeName);
    }

    private dragend(event: DragEvent) {
        if (this.dndManager.draggedData) {
            if (this.dndManager.toList){
                const index = _.findIndex(this.dndManager.toList, (x: any) => x.isPlaceholder);

                if (index > -1){
                    this.dndManager.toList.splice(index, 1);
                }
            }
        }

        this.dataItem.isPlaceholder = false;
        this.dndManager.onDragend();
    }

    private setupEvents() {
        const element = this.elementRef.nativeElement;
        $(element).attr("draggable", "true");
        $(element).on("dragstart", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragstart(event.originalEvent as DragEvent);
            });
        });

        $(element).on("dragend", (event: JQueryEventObject) => {
            this.zone.run(() => {
                this.dragend(event.originalEvent as DragEvent);
            });
        });
    }
}