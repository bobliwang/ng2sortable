import { Component, OnInit } from '@angular/core';
import * as _ from "underscore";
import * as models from './models/Domain.Models';
import {IDndAdapter} from './services/Interfaces'


@Component({
  selector: 'forms-designer',
  template: `

      <div>
        <!-- toolbox begin -->
        <div id="toolbox" style="float:left; width:120px; height: 600px;">

          <div [draggable-item]="fakeControl" sortable-type="Control"
              class="toolboxItem">
            <span>
              {{fakeControl.Type}}
            </span>
          </div>

          <div [draggable-item]="fakeRow" sortable-type="Row"
              class="toolboxItem">
            <span>
              {{fakeRow.Type}}
            </span>
          </div>

          <div [draggable-item]="fakeRepeater" sortable-type="Control"
              class="toolboxItem">
            <span>
              {{fakeRepeater.Type}}
            </span>
          </div>

        </div>

        <!-- toolbox end -->

        <grid-control [grid]="grid"></grid-control>
      
      </div>

      <div style="clear:both"></div>
      <button (click)="showGridData()">Show Data</button>
  `,
})
export class FormsDesignerComponent extends OnInit {
   
  grid = new models.Grid();
  fakeControl = new models.ControlProperties();
  fakeRow = new models.Row();
  fakeRepeater = new models.Repeater();
  
  public ngOnInit() : void {
    this.fakeControl["isToolboxItem"] = true;
    this.fakeRow["isToolboxItem"] = true;
    this.fakeRepeater["isToolboxItem"] = true;

    let row = new models.Row();
    // row.Columns.push(new models.ControlProperties());
    this.grid.Rows.push(row);
    // this.fakeRow.Columns.push(new models.ControlProperties());
  }

  showGridData() {
      console.log(this.grid);
  }


}
