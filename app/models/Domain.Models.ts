
import * as _ from 'underscore'

export class Grid {
    public Rows : Row[];

    public MinWidth: number = 500;

    public Id : string = "Grid_";

    constructor(){
        this.Rows = [];
    }

    public static nextControlId(grid: Grid) : number {
        var id = 0;
        _.each(grid.Rows, (r: Row) => {
            _.each(r.Columns, (c: ControlProperties) => {
                if (id <= c.Seq){
                    id = c.Seq;
                }
            })
        });

        return id + 1;
    }
}

export class Repeater extends Grid {
    public Name : string = "Repeater";

    public Type : string = "Repeater";
}

export class Row {

    public Columns : ControlProperties[];

    constructor() {
        this.Columns = [];
    }

    public Name : string = "Row";

    public Type : string = "Row";
}

export class ControlProperties {
    public Name : string = "Control";
    public Type : string = "Control";
    public Seq : number = 0;

    constructor() {
    }
}